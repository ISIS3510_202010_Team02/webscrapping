const cheerio = require('cheerio');
const request = require('request-promise');
const firestore = require('./pushFirestore');
const fs = require('fs');

async function readGrupoExito(htmldoc) {

  fs.readFile(htmldoc, 'utf8', function (err, data) {

    if (err) throw err;

    const $ = cheerio.load(data);

    let price = ""
    let name = ""
    let img = ""
    let title = $('title').text().split(" - ");
    let category = title[0];
    let store = title[1].trim();


    $('.pa4').each((i, el) => {
      if (i > 3) {
        price = $(el).find('.search-result-exito-vtex-components-selling-price span').text().replace("$", "");
        price= price.replace(".","")
        name = $(el).find('.shelfProductName').text().replace("/", " ");
        img = $(el).find('img').attr('src');
        let numberPrice = parseFloat(price)
        if(isNaN(numberPrice)){
          numberPrice = 10000
        }
        firestore.pushdata(name + " "+ store, name, numberPrice, img, store, category);
        console.log(i, "done:  " + name)
      }
    })
  });
}

async function readAra() {

  const $ = await request({
    uri: 'https://aratiendas.com/el-rebajon-de-precios-region/3+0/',
    transform: body => cheerio.load(body)
  });
  let price = ""
  let name = ""
  let img = ""
  let title = $('title').text().split("|");
  console.log(title)
  let category = "promotion";
  let store = "Ara";

  $(' .views-field-nothing').each((i, el) => {
    if (i > 6) {
      price = $(el).find('.body-producto .precio-naranja').children().next().html().replace(".","");
      name = $(el).find('.footer-producto .descripcion-v').text();
      img = $(el).find('.img-producto img').attr('src');
      const alt = $(el).find('.img-producto img').attr('alt').split(" ", 1);
      firestore.download(img, alt[0] + '.png', function () {
        console.log(img);
      });
      firestore.pushdata(name + " "+ store, name ,parseFloat(price), img, store, category);
      console.log(i, "done:  " + alt[0])
    }
  })

}

loadAllData()
//data load - firesotre
function loadAllData() {
  // en ara solo se muestran las promociones, no tienen un catálogo completo
  readAra()
  //carulla
  readGrupoExito("grupoExitoDocs/carullaLacteos.html")
  readGrupoExito("grupoExitoDocs/carullaAseo.html")
  readGrupoExito("grupoExitoDocs/carullaCarnes.html")
  readGrupoExito("grupoExitoDocs/carullaDespensa.html")
  readGrupoExito("grupoExitoDocs/carullaEnlatados.html")
  readGrupoExito("grupoExitoDocs/carullaFrutas.html")
  readGrupoExito("grupoExitoDocs/carullaVinitos.html")

  //exito
  readGrupoExito("grupoExitoDocs/exitoLacteos.html")
  readGrupoExito("grupoExitoDocs/exitoAseo.html")
  readGrupoExito("grupoExitoDocs/exitoCarnes.html")
  readGrupoExito("grupoExitoDocs/exitoDespensa.html")
  readGrupoExito("grupoExitoDocs/exitoEnlatados.html")
  readGrupoExito("grupoExitoDocs/exitoFrutas.html")
  readGrupoExito("grupoExitoDocs/exitoVinitos.html")
}





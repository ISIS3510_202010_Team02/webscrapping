const request = require('request-promise');
const admin = require('firebase-admin');
var fs = require('fs');


let serviceAccount = require('./unicentrov1ms6-firebase-adminsdk-gr3bd-b20aa5c9e9.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'gs://unicentrov1ms6.appspot.com/'
});

let db = admin.firestore();

//upload an image
function uploadImage(file) {

  // Upload file and metadata to the object 'images/mountains.jpg'
  const uploadTask = storageRef.child('images/' + file.name).put(file);

  // Listen for state changes, errors, and completion of the upload.
  uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
    function (snapshot) {
      // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log('Upload is ' + progress + '% done');
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED: // or 'paused'
          console.log('Upload is paused');
          break;
        case firebase.storage.TaskState.RUNNING: // or 'running'
          console.log('Upload is running');
          break;
      }
    }, function (error) {

      // A full list of error codes is available at
      // https://firebase.google.com/docs/storage/web/handle-errors
      switch (error.code) {
        case 'storage/unauthorized':
          // User doesn't have permission to access the object
          break;

        case 'storage/canceled':
          // User canceled the upload
          break;

        case 'storage/unknown':
          // Unknown error occurred, inspect error.serverResponse
          break;
      }
    }, function () {
      // Upload completed successfully, now we can get the download URL
      uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
        console.log('File available at', downloadURL);
      });
    });

}




//push the data to firestore cloud
module.exports = {
  pushdata: function (id, name, price, img, store, category) {
    let docRef = db.collection("productos2").doc(id);

    let setAda = docRef.set({
      name: name,
      price: price,
      img: img,
      store: store,
      category: category,
      amount: 0
    });
  },
  //download a image
   download: function (uri, filename, callback) {
    request.head(uri, function (err, res, body) {
      console.log('content-type:', res.headers['content-type']);
      console.log('content-length:', res.headers['content-length']);

      request(uri).pipe(fs.createWriteStream("imgAra/" + filename)).on('close', callback);
    });
  }
}




